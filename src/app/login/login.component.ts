import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoginError = false;
  constructor(private userService: UserService,private router: Router) { }

  ngOnInit() {

  }

    OnSubmit(email, password) {

        this.userService.userAuthentication(email, password).subscribe((data: any) => {
                localStorage.setItem('user_token', data.token);
                localStorage.setItem('user_id', data.user.id);
                this.router.navigate(['/contacts']);
            },
            (err: HttpErrorResponse) => {
                this.isLoginError = true;
            });
    }

}
