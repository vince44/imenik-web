import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user.model';
import { Router } from '@angular/router';


@Injectable()
export class UserService {

    constructor(private http: HttpClient, private router: Router) {
    }


    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        }),
        observe: 'response'
    };

    registerUser(user: User) {
        const body: User = {
            name: user.name,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
        };
        return this.http.post('http://46.101.212.47/api/register', body).subscribe((httpOptions) => {
            if (httpOptions) {
                if (true) {
                    alert('Korisnik uspesno kreiran');
                    this.router.navigate(['/login']);

                }
            }
        });

    }

    userAuthentication(email, password) {
        const data = { email: email, password: password};
        const response = this.http.post('http://46.101.212.47/api/login', data);
        return response;

    }

}
