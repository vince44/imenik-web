import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';

import { FormsModule } from '@angular/forms';
import {UserService} from './user.service';
import {HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {RouterModule} from '@angular/router';
import { ContactsComponent } from './contacts/contacts.component';
@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    ContactsComponent
  ],
  imports: [
        BrowserModule,
      FormsModule,
      HttpClientModule,
      RouterModule.forRoot([
          {path: '', component: LoginComponent},
          {path: 'login', component: LoginComponent},
          {path: 'register', component: RegisterComponent},
          {path: 'contacts', component: ContactsComponent}
      ]),
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
