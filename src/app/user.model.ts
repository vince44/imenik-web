export class User {
    name: string;
    password: string;
    email: string;
    password_confirmation: string;
}
