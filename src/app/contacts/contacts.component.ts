import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';



@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css'],
    providers: [UserService]
})
export class ContactsComponent implements OnInit {
    public data: any = [];
    public new_contact: any = {};
  constructor(private userService: UserService, private http: HttpClient, private router: Router) { }
    getContacts(): void {
        const token = localStorage.getItem('user_token');
        const user_id = localStorage.getItem('user_id');
        this.http.get( 'http://46.101.212.47/api/users/' + user_id + '/contacts'
            , {headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
            .subscribe((res: any[]) => {
                    this.data = res;
            });

    }
    editContact(post) {
        const token = localStorage.getItem('user_token');
        const contact = {ime_kontakta: post.ime_kontakta, telefon: post.telefon, email: post.email, id: post.id};
        const response = this.http.post('http://46.101.212.47/api/editContact', contact
            , {headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
            .subscribe((httpOptions) => {
                    if (true) {
                        alert('Kontakt uspesno izmenjen');
                    }

            });
    }
    addContact(new_contact) {
        const token = localStorage.getItem('user_token');
        const contact = {
            ime_kontakta: new_contact.
            ime_kontakta, telefon: new_contact.
            telefon, email: new_contact.email
        };
        const response = this.http.post('http://46.101.212.47/api/insertContact', contact
            , {headers: new HttpHeaders({'Authorization': 'Bearer ' + token})})
            .subscribe((httpOptions) => {
                if (true) {
                    alert('Kontakt uspesno kreiran');
                    this.ngOnInit();
                    this.new_contact = {};

                }

            });
  }
    isBtnDisabled() {
      console.log(this.new_contact);
        if (typeof this.new_contact.ime_kontakta === 'undefined' || this.new_contact.ime_kontakta === '') {
            return true;
        }
        if (typeof this.new_contact.telefon === 'undefined' || this.new_contact.telefon === '') {
            return true;
        }
        if (typeof this.new_contact.email === 'undefined' || this.new_contact.email === '') {
            return true;
        }
      return false;
    }

  ngOnInit(): void {
      if (localStorage.getItem('user_token') === null ) {
          this.router.navigate(['/login']);
      }
      this.getContacts();
  }

}
