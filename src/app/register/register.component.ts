import { Component, OnInit } from '@angular/core';
import {User} from '../user.model';
import {NgForm} from '@angular/forms';
import {UserService} from '../user.service';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = new User();
  constructor(private userService: UserService) { }

  ngOnInit() {
    
  }

  resetForm(form: NgForm) {

    if (form != null) {
        form.reset();
        this.user = {
            name: '',
            password: '',
            email: '',
            password_confirmation: ''
        };
    }
  }

  OnSubmit(form: NgForm) {
    this.userService.registerUser(form.value);
  }
}
